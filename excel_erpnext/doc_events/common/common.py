import frappe
# import locale
from frappe.utils import get_url

from datetime import datetime
# try:
#     locale.setlocale(locale.LC_ALL, 'en_IN.UTF-8')
# except locale.Error:
#     locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

def get_customer_details(customer_name_id,outstanding_balance=False):
    customer = frappe.get_doc("Customer", customer_name_id)
    customer_name = customer.customer_name
    is_frozen=customer.is_frozen
    # Check sales team presence
    sales_team_name = (
        customer.sales_team[0].sales_person
        if hasattr(customer, 'sales_team') and customer.sales_team and len(customer.sales_team) > 0
        else "Unknown Sales Person"
    )
    
    # Fetch sales person details
    get_sales_person_details = frappe.db.sql("""
        SELECT name, excel_sales_person_email, excel_sales_person_mobile_no 
        FROM `tabSales Person` 
        WHERE name = %s
    """, sales_team_name, as_dict=True)
    
    if get_sales_person_details:
        excel_sales_person_name = get_sales_person_details[0].get('name', '')
        excel_sales_person_mobile_no = get_sales_person_details[0].get('excel_sales_person_mobile_no', '')
        excel_sales_person_email = get_sales_person_details[0].get('excel_sales_person_email', '')
    else:
        excel_sales_person_name = ""
        excel_sales_person_mobile_no = ""
        excel_sales_person_email = ""
    excel_sales_person_name = get_sales_person_details[0]['name']
    excel_sales_person_mobile_no = get_sales_person_details[0]['excel_sales_person_mobile_no']
    excel_sales_person_email = get_sales_person_details[0]['excel_sales_person_email']
    customer_primary_contact = customer.customer_primary_contact
    customer_fixed_limit = customer.excel_fixed_credit_limit
    customer_primary_contact_phone_no = get_notified_mobile_no(customer_primary_contact)
    customer_primary_contact_email = get_notified_email(customer_primary_contact)
    customer_outstanding_balance = get_customer_outstanding_balance(customer_name_id) if outstanding_balance else 0
    return {'sales_person_name':excel_sales_person_name,
            'sales_person_mobile_no':excel_sales_person_mobile_no,
            'sales_person_email':excel_sales_person_email,
            'notified_phone_no_list':customer_primary_contact_phone_no,
            'notified_email_list':customer_primary_contact_email,
            'outstanding_balance':customer_outstanding_balance,
            'fixed_limit':customer_fixed_limit,
            'customer_name':customer_name,
            'is_frozen':True if is_frozen else False
            }


def get_notified_mobile_no(primary_contact):
    # Raw SQL query to fetch the contact phone
    query = """
        SELECT cp.phone 
        FROM `tabContact Phone` AS cp
        WHERE cp.parenttype = 'Contact'
        AND cp.parentfield = 'phone_nos'
        AND cp.excel_notification=1
        AND cp.parent = %s
    """
    result = frappe.db.sql(query, primary_contact, as_dict=True)
    phone_nos = [row['phone'] for row in result]
    return phone_nos
def get_notified_email(primary_contact):
    query = """
        SELECT cp.email_id 
        FROM `tabContact Email` AS cp
        WHERE cp.parenttype = 'Contact'
        AND cp.parentfield = 'email_ids'
        AND cp.excel_notification=1
        AND cp.parent = %s
    """
    result = frappe.db.sql(query, primary_contact, as_dict=True)
    emails = [row['email_id'] for row in result]
    return emails


def get_customer_outstanding_balance(customer_name):
    total_debit_credit = frappe.db.sql(
        """
        SELECT 
            SUM(debit_in_account_currency) AS total_debit,
            SUM(credit_in_account_currency) AS total_credit
        FROM 
            `tabGL Entry`
        WHERE 
            party_type = 'Customer' AND party = %s
        """, 
        (customer_name,),
        as_dict=True
    )
    
    # Calculate the total outstanding balance
    total_debit = total_debit_credit[0].get('total_debit') or 0
    total_credit = total_debit_credit[0].get('total_credit') or 0
    outstanding_balance = total_debit - total_credit
    
    return outstanding_balance

def format_in_bangladeshi_currency(amount, sms=False,is_abs=True):
    if isinstance(amount, str):
        amount = float(amount)
    # Determine if the amount is negative
    is_negative = amount < 0
    if is_negative:
        amount = abs(amount)  # Work with the positive version of the amount for formatting
    
    # Round the amount to 2 decimal places or 1 decimal place if sms=True
    amount = round(amount, 2)

    # Convert to string for formatting
    amount_str = str(amount)

    # Split the amount into whole and decimal parts
    if '.' in amount_str:
        whole_part, decimal_part = amount_str.split('.')
    else:
        whole_part, decimal_part = amount_str, '0'

    # Format the whole part with commas for Bangladeshi style
    whole_length = len(whole_part)
    if whole_length > 3:
        # First part, split it by 3 digits
        first_part = whole_part[-3:]
        remaining_part = whole_part[:-3]

        # Insert commas every 2 digits from the left side
        formatted_remaining_part = []
        while len(remaining_part) > 2:
            formatted_remaining_part.append(remaining_part[-2:])
            remaining_part = remaining_part[:-2]

        # Add the final part
        formatted_remaining_part.append(remaining_part)

        # Reassemble the whole part with commas
        formatted_whole_part = ','.join(formatted_remaining_part[::-1]) + ',' + first_part
    else:
        formatted_whole_part = whole_part

    # Return the final formatted string
    return f"{'-' if not is_abs and is_negative else ''}{   formatted_whole_part}.{decimal_part}/="
# def format_in_bangladeshi_currency(amount, sms=False, use_abs=True):
#     if isinstance(amount, str):
#         amount = float(amount)
    
#     # Determine if the amount is negative
#     is_negative = amount < 0
#     if is_negative and use_abs:
#         amount = abs(amount)  # Work with the positive version of the amount for formatting
    
#     # Round the amount to 2 decimal places
#     amount = round(amount, 2)

#     # Convert to string for formatting
#     amount_str = str(amount)

#     # Split the amount into whole and decimal parts
#     if '.' in amount_str:
#         whole_part, decimal_part = amount_str.split('.')
#     else:
#         whole_part, decimal_part = amount_str, '0'

#     # Format the whole part with commas for Bangladeshi style
#     whole_length = len(whole_part)
#     if whole_length > 3:
#         first_part = whole_part[-3:]
#         remaining_part = whole_part[:-3]

#         formatted_remaining_part = []
#         while len(remaining_part) > 2:
#             formatted_remaining_part.append(remaining_part[-2:])
#             remaining_part = remaining_part[:-2]

#         formatted_remaining_part.append(remaining_part)

#         formatted_whole_part = ','.join(formatted_remaining_part[::-1]) + ',' + first_part
#     else:
#         formatted_whole_part = whole_part

#     return f"{'-' if not use_abs and is_negative else ''}{formatted_whole_part}.{decimal_part}/="

def get_notification_permission(customer):
    sms=False
    email=False
    both=False
    try:
        settings = frappe.get_doc("ArcApps Alert Settings")
        customer_details = frappe.get_doc("Customer",customer)
        sms_enabled = bool(settings.excel_sms)
        email_enabled = bool(settings.excel_email)
        if customer_details.excel_notification_type == "SMS" and sms_enabled:
            sms = True
        elif customer_details.excel_notification_type == "Email" and email_enabled:
            email = True
        elif customer_details.excel_notification_type == "SMS & Email":
            both = True
        return {'sms':sms,'email':email,'both':both}
    except frappe.DoesNotExistError:
        return {'sms':False,'email':False,'both':False}
    
    
def format_time_to_ampm(time_string,is_mail=False):
    try:
        # Convert to datetime object
        time_obj = datetime.strptime(time_string, '%Y-%m-%d %H:%M:%S.%f')
        
        # Format the time as 05:23PM without space
        formatted_time = time_obj.strftime('%I:%M%p') if not is_mail else time_obj.strftime('%I:%M %p')
        
        return formatted_time

    except ValueError:
        return "Invalid time format"
def format_date_to_custom(date_input, need_year=False):
    # frappe.msgprint(date_input)
    try:
        # Check if the input is already a date object
      
            # Convert string to datetime object
        date_obj = datetime.strptime(date_input, '%Y-%m-%d')
        
        # Format the date as '24-Oct-24' or '24-Oct-2024'
        formatted_date = date_obj.strftime('%d-%b-%y') if not need_year else date_obj.strftime('%d-%b-%Y')
        # frappe.msgprint(formatted_date)
        
        return formatted_date
    except Exception as e:
        # Handle exceptions (e.g., invalid date format)
        print(f"Error formatting date: {e}")
        return None
    
def format_date_to_custom_cancel(date_input, need_year=False):
    # frappe.msgprint(date_input)
    try:
        # Check if the input is already a date object
      
            # Convert string to datetime object
        date_obj = datetime.strptime(date_input, '%Y-%m-%d %H:%M:%S.%f')
        
        # Format the date as '24-Oct-24' or '24-Oct-2024'
        formatted_date = date_obj.strftime('%d-%b-%y') if not need_year else date_obj.strftime('%d-%b-%Y')
        # frappe.msgprint(formatted_date)
        
        return formatted_date
    except Exception as e:
        # Handle exceptions (e.g., invalid date format)
        print(f"Error formatting date: {e}")
        return None
def get_attachment_permission(doc_name):
    if doc_name == "Journal Entry": 
        settings = frappe.get_doc("ArcApps Alert Settings")
        return bool(settings.journal_att)
    elif doc_name == "Payment Entry":
        settings = frappe.get_doc("ArcApps Alert Settings")
        return bool(settings.payment_att)
    elif doc_name == "Sales Invoice":
        settings = frappe.get_doc("ArcApps Alert Settings")
        return bool(settings.invoice_att)
    elif doc_name == "Journal Attachment (Corporate)":
        settings = frappe.get_doc("ArcApps Alert Settings")
        return bool(settings.corp_journal_att)
    else:
        return False
    
    
def check_allow_on_doctype():
    settings = frappe.get_doc("ArcApps Alert Settings")
    return {
        "sales_invoice":bool (settings.sales_invoice),
        "sales_return": bool(settings.sales_return),
        "payment_entry":bool(settings.payment_entry),
        "credit_note_journal":bool(settings.credit_note_journal),
        "benefit_journal":bool(settings.benefit_journal),
        "debit_adjustment_journal":bool(settings.debit_adjustment_journal),
        "receive_journal":bool(settings.receive_journal),
        "cancellation_all":bool(settings.cancellation_all)
    }

def get_cm_mail():
    try:
        # Fetch the settings document
        settings = frappe.get_doc("ArcApps Alert Settings")
        mail_list = settings.get("mail_list", [])
        if not mail_list:
            frappe.log_error("Mail list is empty or missing in ArcApps Alert Settings", "Missing Mail List")
            return []
        user_ids = [mail.get("user_email") for mail in mail_list if mail.get("user_email")]      
        return user_ids

    except Exception as e:
        # Log the error for debugging
        frappe.log_error(frappe.get_traceback(), "Error in get_cm_mail")
        print(f"Error: {str(e)}")
        return []
    
def send_cm_mail_from_payment_entry(doc ,voucher_name):
    settings = frappe.get_doc("ArcApps Alert Settings")
    if not bool(settings.payment_entry_cm):
        return
    customer = doc.party
    paid_amount = doc.paid_amount
    customer_details = get_customer_details(customer,outstanding_balance=True)
    is_frozen = customer_details.get('is_frozen')
    if not is_frozen:
        return
    customer_name= customer_details.get('customer_name')
    outstanding_balance= customer_details.get('outstanding_balance')
    fixed_limit = customer_details.get('fixed_limit')
    send_email_to_cm(customer,customer_name,paid_amount,outstanding_balance,fixed_limit,enrty_type='payment entry',voucher_name=voucher_name)
    
def send_cm_mail_from_journal_entry(customer,voucher_name):
    settings = frappe.get_doc("ArcApps Alert Settings")
    if not bool(settings.journal_entry_cm):
        return    
    account = customer.get('account')
    party_type = customer.get('party_type')
    party_name = customer.get('party')
    
    customer_details = get_customer_details(party_name, outstanding_balance=True)
    is_frozen = customer_details.get('is_frozen')
    if not is_frozen:
        return
    customer_name= customer_details.get('customer_name')
    outstanding_balance= customer_details.get('outstanding_balance')
    credit_amount = customer.get('credit_in_account_currency')
    debit_amount = customer.get('debit_in_account_currency')
    fixed_limit = customer_details.get('fixed_limit')
    if credit_amount > 0:
        paid_amount = credit_amount
    else:
        return
    send_email_to_cm(party_name,customer_name,paid_amount,outstanding_balance,fixed_limit,enrty_type='journal entry',voucher_name=voucher_name)



def send_email_to_cm(customer_code, customer_name, paid_amount, outstanding_balance,fixed_limit,enrty_type='payment entry',voucher_name=None):
    subject = 'Customer Unfreeze Alert'
    base_url = get_url()
    mail_list = get_cm_mail()
    if len(mail_list) == 0:
        return
    # Generate the customer URL
    customer_url = f"{base_url}/app/customer/{customer_code}"
    if enrty_type == 'payment entry':
        voucher_url = f"{base_url}/app/payment-entry/{voucher_name}"
    elif enrty_type == 'journal entry':
        voucher_url = f"{base_url}/app/journal-entry/{voucher_name}"
    message = f"""
    <p>Dear Concern,</p>
    
    <p>A {enrty_type} <strong><a href="{voucher_url}" target="_blank">{voucher_name}</a></strong> has been submitted for <strong>{customer_name}</strong>. Kindly review and take the necessary action to unfreeze.</p>
    
    <p><strong>Details:</strong></p>
    <ul>
        <li>Customer: <a href="{customer_url}" target="_blank">{customer_code}</a></li>
        <li>Customer Name: <strong>{customer_name}</strong></li>
        <li>Paid Amount: <strong>{format_in_bangladeshi_currency(paid_amount)} Taka</strong></li>
        <li>Fixed Credit Limit: <strong>{format_in_bangladeshi_currency(fixed_limit)} Taka</strong></li>
        <li>Current Outstanding: <strong>{format_in_bangladeshi_currency(outstanding_balance)} Taka</strong></li>
        
    </ul>
    
    <p>Best Regards,<br>Team ETL</p>
    """
    
    # Displaying the message for debug purposes (optional)
  
    frappe.sendmail(
        recipients=mail_list,
        subject=subject,
        message=message
    )


def generate_transaction_table(transaction_details):
    """
    Generate an HTML email with a dynamic transaction table.

    :param transaction_details: Dictionary containing transaction details
    :return: HTML email string
    """
    table_rows = ""
    for key, value in transaction_details.items():
        table_rows += f"""
            <tr>
                <td style="padding: 5px; text-align: left; border: 1px solid #ddd; background-color: #f9f9f9;"><b>{key}</b></td>
                <td style="padding: 5px; text-align: left; border: 1px solid #ddd; background-color: #f9f9f9;">{value}</td>
            </tr>
        """

    email_html = f"""
    <p>Dear <b>Valued Partner</b>,</p>
    <p>We truly appreciate your continued business and partnership. We have performed the following transaction, details stated below:</p>
    <br>
    <table border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; width: 650px; table-layout: fixed;">
    <tr style="background-color: #2c63af;">
        <td style="color: #ffffff; padding: 5px; text-align: left; border: 1px solid #ddd;"><b>Particulars</b></td>
        <td style="color: #ffffff; padding: 5px; text-align: left; border: 1px solid #ddd;"><b>Transaction Details</b></td>
    </tr>
        {table_rows}
    </table>
    <style>
    @media (max-width: 600px) {{
        table {{
            width: 100% !important;
        }}
        td {{
            width: 100% !important;
            display: block;
            box-sizing: border-box;
        }}
        td:first-child {{
            font-weight: bold;
            padding-top: 10px;
        }}
        td:nth-child(2) {{
            padding-top: 10px;
        }}
    }}
    </style>
    """

    return email_html



# Example usage


def generate_email_footer():
    """
    Generate an HTML footer for the email.
    
    :return: HTML footer string
    """
    footer_html = """
   
    <p >For more information on our products and services, please visit our website: 
        <a href="http://www.excelbd.com" target="_blank">www.excelbd.com</a> 
        or on Facebook: 
        <a href="https://www.facebook.com/ExcelTechnologiesLtd" target="_blank">Excel Technologies Ltd</a>
    </p>
    
    <p style="padding-top: 20px; margin: 0px !important;">Sincerely,</p>
    <p style="margin: 0px !important;">Excel Technologies Ltd.</p>
    <p style="color: #888; font-size: 12px; font-style: italic;">
        This is a system-generated email. Please do not reply, as responses to this email are not monitored.
    </p>
    """
    return footer_html



def generate_contact_info(sales_person_name = None, sales_person_mobile_no=None, sales_person_email=None):
    """
    Generate an HTML paragraph for contact information.

    :param sales_person_name: Name of the sales representative
    :param sales_person_mobile_no: Mobile number of the sales representative (optional)
    :param sales_person_email: Email of the sales representative (optional)
    :return: HTML string with contact information
    """
    contact_info = f"""
    <p>If you have any requirements or need assistance, please feel free to reach out 
    {'your KAM' if not sales_person_mobile_no and not sales_person_email else 'to'} 
    <b>{sales_person_name}</b> 
    {'.' if not sales_person_mobile_no and not sales_person_email else ''}
    {f'at {sales_person_mobile_no}' if sales_person_mobile_no else ''}
    {f'or email' if '<a href="mailto:{sales_person_email}" >{sales_person_email}</a>' and sales_person_mobile_no else ''}
    {f'at <a href="mailto:{sales_person_email}" >{sales_person_email}</a>' if sales_person_email else ''} 
    </p>
    
    """
    return contact_info.strip()


# Example usage

